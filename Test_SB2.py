# Tests SB2 - Question 1.2.6
from SB2 import *

# CHAPTER 1

# 1.2.1
def test_read_interaction_file_dict():
    """
    Tests of SB2.read.interaction_file_dict(filename).
    """
    # Tests on toy example :
    assert(read_interaction_file_dict("toy_example.txt")["A"] == ["B", "C"]), \
        f"A interactions should be BC."
    assert(read_interaction_file_dict("toy_example.txt")["B"] == ["A", "C", "D"]), \
        f"B interactions should be ACD."
    assert not(read_interaction_file_dict("toy_example.txt")["F"] == ["A", "D"]), \
        f"F has no interaction with A."
    # Tests on Human_HighQuality :
    assert(read_interaction_file_dict("Human_HighQuality.txt")
        ["1A02_HUMAN"] == ["B2MG_HUMAN"]), f"10A2 interact with B2MG only."
    assert not(read_interaction_file_dict("Human_HighQuality.txt")
        ["1A68_HUMAN"] == ["BAP31_HUMAN"]), f"1068 interact with more partners."
    return "1.2.1 : All tests passed !"

print(test_read_interaction_file_dict())


# 1.2.2
def test_read_interaction_file_list():
    """
    Tests of SB2.read.interaction_file_list(filename).
    """
    # Tests on toy_example :
    assert(len(read_interaction_file_list("toy_example.txt")) == 6), \
        f"The file contains 6 intercations."
    assert(read_interaction_file_list("toy_example.txt")[0] == ["A", "B"]), \
        f"First pair should be A B."
    assert(read_interaction_file_list("toy_example.txt")[5] == ["D", "F"]), \
        f"Last pair should be D F."
    # Tests on Human_HighQuality :
    assert(len(read_interaction_file_list("Human_HighQuality.txt")) == 27276), \
        f"The file contains 27276 intercations."
    assert(read_interaction_file_list("Human_HighQuality.txt")
        [27275] == ["ZYX_HUMAN", "ZN384_HUMAN"]), \
        f"First pair should be ['ZYX_HUMAN', 'ZN384_HUMAN']."
    return "1.2.2 : All tests passed !"

print(test_read_interaction_file_list())


# 1.2.3
def test_read_interaction_file_mat():
    """
    Tests of SB2.read_interaction_file_mat(filename).
    """
    # Tests on toy_example :
    assert(read_interaction_file_mat("toy_example.txt")[0][0][0] == 0), \
        f"The first value of the matrix should be 0."
    assert(read_interaction_file_mat("toy_example.txt")[0][5][5] == 0), \
        f"The last value of the matrix should be 0."
    assert(read_interaction_file_mat("toy_example.txt")[0][2][1] == 1), \
        f"The interaction between B and C is True, should be = 1."
    assert(len(read_interaction_file_mat("toy_example.txt")[1]) == 6), \
        f"The file contains 6 intercations."
    assert("A" in read_interaction_file_mat("toy_example.txt")[1]), \
        f"The first vexter should be A."
    assert("F" in read_interaction_file_mat("toy_example.txt")[1]), \
        f"The last vexter should be F."
    assert not("H" in read_interaction_file_mat("toy_example.txt")[1]), \
        f"The last vexter should be F."
    return "1.2.3 : All tests passed !"

print(test_read_interaction_file_mat())


# 1.2.4
def test_read_interaction_file():
    """
    Tests of SB2.read_interaction_file(filename).
    """
    # Tests on toy_example :
    assert(len(read_interaction_file("toy_example.txt")) == 4), \
        f"The length of the tuple should be 4"
    assert(type(read_interaction_file("toy_example.txt")[0]) == dict), \
        f"The first element of the tuple should be a dictionnary"
    assert(type(read_interaction_file("toy_example.txt")[1]) == list), \
        f"The second element of the tuple should be a list"
    # Tests on Human_HighQuality :
    assert(len(read_interaction_file("Human_HighQuality.txt")) == 4), \
        f"The length of the tuple should be 4"
    return "1.2.4 : All tests passed !"

print(test_read_interaction_file())


# 1.2.7
def test_is_interaction_file():
    """
    Tests of SB2.is_interaction_file(filename).
    """
    assert(is_interaction_file("toy_example.txt")), \
        f"toy_example should be a correct inputfile"
    assert(is_interaction_file("Human_HighQuality.txt")), \
        f"Human_HighQuality should be a correct inputfile"
    assert not(is_interaction_file("test.txt")), \
        f"test.txt should not be a correct inputfile"
    return "1.2.7 : All tests passed !"

print(test_is_interaction_file())


#  CHAPTER 2

# 2.1.1
def test_count_vertices():
    """
    Tests of SB2.count_vertices(filename).
    """
    assert(count_vertices("toy_example.txt") == 6), \
        f"There is 6 vertices in toy_example"
    return "2.1.1 : All tests passed !"

print(test_count_vertices())


# 2.1.2
def test_count_edges():
    """
    Tests of SB2.count_edges(filename).
    """
    assert(count_edges("toy_example.txt") == 6), \
        f"There is 6 edges in toy_example."
    assert(count_edges("Human_HighQuality.txt") == 27276), \
        f"The is 27276 edges in toy_example."
    return "2.1.2 : All tests passed !"

print(test_count_edges())


# 2.1.3
def test_clean_interactome():
    """
    Tests of SB2.count_edges(filename).
    """
    clean_interactome("toy_example.txt", "toy_example_v2.txt")
    assert(is_interaction_file("toy_example_v2.txt")), \
        f"The file generated is not correct."
    assert(count_vertices("toy_example_v2.txt") == 6), \
        f"There is 6 vertices in toy_example_v2."
    assert(count_edges("toy_example_v2.txt") == 6), \
        f"There is 6 edges in toy_example_v2."
    return "2.1.3 : All tests passed !"

print(test_clean_interactome())


# 2.2.1
def test_get_degree():
    """
    Tests of SB2.get_degree(filename, prot).
    """
    assert(get_degree("toy_example.txt", "A") == 2 ), \
        f"A has 2 neighbors."
    assert(get_degree("toy_example.txt", "B") == 3 ), \
        f"B has 3 neighbors."
    assert(get_degree("Human_HighQuality.txt", "1433B_HUMAN") == 49), \
        f"1433B_HUMAN has 49 neigbors."
    assert(get_degree("Human_HighQuality.txt", "1433E_HUMAN") == 50), \
        f"1433E_HUMAN has 50 neigbors."
    return "2.2.1: All tests passed !"

print(test_get_degree())

# 2.2.2
def test_get_max_degree():
    """
    Tests of SB2.get_max_degree(filename).
    """
    assert(get_max_degree("toy_example.txt") == "B D"), \
        f"B and D are the proteins with the highest degree."
    return "2.2.2: All tests passed !"

print(test_get_max_degree())

# 2.2.3
def test_ave_degree():
    """
    Tests of SB2.ave_degree(filename).
    """
    assert(get_ave_degree("toy_example.txt") == 2), \
        f"The average degree of the proteins in toy_example.txt is 2."
    return "2.2.3: All tests passed !"

print(test_ave_degree())

# 2.2.4
def test_count_degree():
    """
    Tests of SB2.count_degree(filename, deg).
    """
    assert(count_degree("toy_example.txt", 3) == 2), \
        f"There is only 2 proteins with a degree of 3 in toy_example.txt."
    assert(count_degree("toy_example.txt", 1) == 2), \
        f"There is 3 porteins with a degree of 1 in toy_example.txt"
    return "2.2.4: All tests passed !"

print(test_count_degree())

# 2.2.5
#def test_histogram_degree():
#    """
#    Tests of SB2.histogram_degree(filename, dmin, dmax).
#    """
#    assert(histogram_degree("toy_example.txt", 1, 3) == \
#        "1 **\n2 **\n3 **\n"), f"incorrect histogram."
#    return "2.2.5: All tests passed !"

#print(test_histogram_degree())