# Project SB2

# Christelle Lalanne (IBI)
# Alexandre Pastre (IBI)

# Imports
from ctypes import Array
import os
import numpy as np
from array import array
import re

# CHAPTER 1

# 1.2.1
def read_interaction_file_dict(filename) -> dict:
    """
    Reads an input file describing protein-protein interactions and stores
    those interactions in a dictionnary.

    Ignores the first line and reads each line of the input file.
    Retrieve each proteins of the graph as dictionary key.
    The neighbors of these proteins constitute the values associated
    to the keys.

    Args:
        filename (file): The input file describing protein-protein interactions.

    Returns:
        dict: Contains the interactions described in the input file.
    """
    interaction_dict = {}
    with open(filename, "r") as fh:
        next(fh)
        for line in fh:
            elem = re.split('\t| |,|\n', line)
            if elem[0] not in interaction_dict.keys():
                interaction_dict[elem[0]] = [elem[1]]
            else:
                if elem[1] not in interaction_dict[elem[0]] :
                    interaction_dict[elem[0]] += [elem[1]]
            if elem[1] not in interaction_dict.keys():
                interaction_dict[elem[1]] = [elem[0]]
            else:
                if elem[0] not in interaction_dict[elem[1]] :
                    interaction_dict[elem[1]] += [elem[0]]
        return interaction_dict


# 1.2.2
def read_interaction_file_list(filename) -> list:
    """
    Reads an input file describing protein-protein interactions and stores
    those interactions in a list.

    Ignores the first line and reads each line of the input file.
    Add each pairs of proteins in 'total_couples_list'

    Args:
        filename (file): The input file describing protein-protein interactions.

    Returns:
        list: Contains the each proteins pairs described in the input file.
    """
    total_couples_list = []
    with open(filename, "r") as fh:
        next(fh)
        for line in fh:
            couple_list = []
            elem = re.split('\t| |,|\n', line)
            couple_list.append(elem[0])
            couple_list.append(elem[1])
            total_couples_list.append(couple_list)
    return total_couples_list


# 1.2.3
def read_interaction_file_mat(filename):
    """
    Reads an input file describing protein-protein interactions and stores
    each vertices in an array.

    Using the function read_interaction_file_dict to retrieve the interactions.
    Builds an array ordered and based on the previous dictionnary.

    Args:
        filename (file): The input file describing protein-protein interactions.

    Returns:
        array : Array associated to the graph
        list : Contains all the vertices ordered.
    """
    interaction_dict = read_interaction_file_dict(filename)
    mat = []
    vertices = interaction_dict.keys()
    for i in vertices:
        line = [int(j in interaction_dict[i]) for j in vertices]
        mat.append(line)
    return (np.array(mat), vertices)


# 1.2.4
def read_interaction_file(filename) -> tuple:
    """
    Returns a tuple with the graph dictionnary, the interactions list,
    the graph array and the ordered vertices list.

    Args:
        filename (file): The input file describing protein-protein interactions.

    Returns:
        tuple: Contains the graph dictionnary, the interactions list,
        the graph array and the ordered vertices list.
    """
    d_int = read_interaction_file_dict(filename)
    l_int = read_interaction_file_list(filename)
    m_int = read_interaction_file_mat(filename)[0]
    l_som = read_interaction_file_mat(filename)[1]
    return (d_int, l_int, m_int, l_som)


# 1.2.5
# ??


# 1.2.6
# See Tests_SB2.py


# 1.2.7
def is_interaction_file(filename) -> bool:
    """
    Checks the if the input file is written with the correct format.

    Checks in the first place if the file is empty.
    Then checks if the first line stating the number of protein-protein interactions is present and correct.
    Finally, checks that protein-protein interactions described in the given file only take into account two proteins at a time.

    Args:
        filename (file): The input file describing protein-protein interactions.

    Returns:
        bool: True if the input file is correct, False otherwise.
    """
    if os.stat(filename).st_size == 0:
        return False
    with open(filename, "r") as fh:
        all_lines_list = fh.readlines()
        interaction_count = len(all_lines_list)-1
        first_line = str(interaction_count)+"\n"
        if all_lines_list[0] != first_line:
            return False
        for i in all_lines_list[1:]:
            j = re.split('\t| |, ', i)
            if len(j) != 2:
                return False
    return True


# CHAPTER 2

# 2.1.1
def count_vertices(file) -> int:
    """
    Counts the number of vertices in a graph

    Args:
        file (file): The input file describing protein-protein interactions.

    Returns:
        int: Number of vertices in the graph
    """
    return len(read_interaction_file_dict(file))


# 2.1.2
def list_without_duplicates(file) -> list:
    """
    Retrieve all the unique couples presents in 'l_int'.

    Create a cleaned list based on 'l_int'.

    Args:
        file (file): The input file describing protein-protein interactions.

    Returns:
        list: Contains uniques couples. 
    """
    total_couples_list = read_interaction_file_list(file)
    seen_couples_list = []
    no_duplicates_list = []
    for i in total_couples_list:
        if i not in seen_couples_list:
            no_duplicates_list.append(i)
            seen_couples_list.append(i)
            couple_str_reversed = [i[1], i[0]]
            seen_couples_list.append(couple_str_reversed)
    return no_duplicates_list


def count_edges(file) -> int:
    """
    Counts the number of edges in a graph.

    Args:
        file (file): The input file describing protein-protein interactions.

    Returns:
        int: Number of edges in the graph.
    """
    return len(list_without_duplicates(file))


# 2.1.3
def clean_interactome(filein, fileout):
    """
    Writes a clean graph file without duplicates or homo-dimer.

    Call the function "list_without_duplicate" to retrieve a list
    without duplicates.
    Based on this list, a final clean list is created, containing no homo-dimers.
    Write the output file based on this list.

    Args:
        filein (file): The input file describing protein-protein interactions.
        fileout (str): Name of the generated file.

    Returns:
        file : A generated file store in the current directory.
    """
    with open(fileout, "w") as out:
        no_duplicates_list = list_without_duplicates(filein)
        final_clean_list = []
        for i in no_duplicates_list:
            if i[0] != i[1] :
                final_clean_list.append(i)
        out.write(str(len(final_clean_list))+"\n")
        for i in final_clean_list[:-1]:
            out.write(i[0]+" "+i[1]+"\n")
        out.write(final_clean_list[-1][0]+" "+final_clean_list[-1][1])


# 2.1.4
# See Tests_SB2.py


# 2.2.1
def get_degree(file, prot : str) -> int :
    """
    Return the degree of a proteine present in the graph.

    Check if "prot" is correct.

    Call the function "read_interaction_file_dict" in order to read 
    the interractions file.
    Using the key "prot" in the dictionnary to retrieve all the neighbors
    which are the values associated of the keys.

    Args:
        file (file): The input file describing protein-protein interactions.
        prot (str): The protein for which we are looking for the degree.

    Returns:
        int: The degree of the protein "prot".
    """
    if not isinstance(prot, str):
        raise TypeError("prot should be a string")
    if prot not in read_interaction_file_dict(file).keys():
        raise KeyError(f"{prot} is not in {file}")
    return len(read_interaction_file_dict(file)[prot])


# 2.2.2
def get_max_degree(file) -> str:
    """
    Return the name of the protein with the highest degree.

    Call the function "read_interaction_file_dict" in order to read 
    the interractions file.
    Using the function get_degree for calculing the degree of a protein.
    Keep the protein (=key) in memory only if his degree is higher compared
    to the previous one.

    Args:
        file (file): The input file describing protein-protein interactions.

    Returns:
        str: The name of the protein with the highest degree.
        int : The highest degree.
    """
    dictio = read_interaction_file_dict(file)
    prot_str = ""
    nb_neighbor_int = 0
    for key in dictio :
        tmp_int = get_degree(file, key)
        if tmp_int > nb_neighbor_int :
            nb_neighbor_int = tmp_int
            prot_str = key
        elif tmp_int == nb_neighbor_int :
            prot_str += " " + key
    return prot_str, get_degree(file,str(prot_str[0]))


# 2.2.3
def get_ave_degree(file) -> float :
    """
    Calculate the average degree for the contained proteins in the graph.

    Call the function "read_interaction_file_dict" in order to read 
    the interractions file.
    Using the function get_degree to retrieve all the degrees for each
    proteins in the graph. Sum them up and divide by the length of "dico"
    to get the mean.

    Args:
        file (file): The input file describing protein-protein interactions.

    Returns:
        float: The average degree for the contained proteins in the graph.
    """
    dictio = read_interaction_file_dict(file)
    total_int = 0
    for key in dictio :
        total_int += get_degree(file, key)
    return total_int/len(dictio)


# 2.2.4
def count_degree(file, deg : int) -> int:
    """
    Count the number of proteins in the graph with the same degreea as "deg".

    Check the correct value and type of deg.

    Call the function "read_interaction_file_dict" in order to read 
    the interractions file.
    Using the function get_degree to retrieve all the degrees for each
    proteins in the graph. Each time a protein has the same degree as "deg",
    add plus one to the result.

    Args:
        file (file): The input file describing protein-protein interactions.
        deg (int): The degree of interest.

    Returns:
        int: The number of proteins with the same degree as "deg".
    """
    if not isinstance(deg, int) :
        raise TypeError("deg should be an integer")
    if deg <= 0 :
        raise ValueError("deg should be positive")
    dictio = read_interaction_file_dict(file)
    count_int = 0
    for key in dictio :
        tmp_int = get_degree(file, key)
        if tmp_int == deg :
            count_int += 1
    return count_int


# 2.2.5
def histogram_degree(file, dmin : int , dmax : int) -> dict :
    """
    Draw an histogram of the number of proteins per degree.

    Check the correct value and type of dmin and dmax.

    Call the function "read_interaction_file_dict" in order to read 
    the interractions file.
    Using the function get_degree to retrieve all the degrees for each
    proteins in the graph. Each time a protein has the same degree as between
    dmin and dmax, add plus one "*" to the corresponding dictionnary keys.

    Args:
        file (file): The input file describing protein-protein interactions.
        dmin (int): The minimum degree of interest.
        dmax (int): The maximum degree of interest.

    Returns:
        dict: A dictionnary containing the number of proteins (values) per 
        degree (keys).
        The histogram is drawn by using this dictionnary.
    """
    if not isinstance(dmin, int) or not isinstance(dmax, int) :
        raise TypeError("dmin or/and dmax should be an integer")
    if dmin <= 0 or dmax <= 0 :
        raise ValueError("dmin or/and dmax should be positive")
    hist_dict = dict()
    dictio = read_interaction_file_dict(file)
    for key in dictio :
        tmp_int = get_degree(file, key)
        if tmp_int >= dmin and tmp_int <= dmax :
            if tmp_int not in hist_dict :
                hist_dict[tmp_int] = "*"
            else :
                hist_dict[tmp_int] += "*"
    for key in sorted(hist_dict):
        print (key, hist_dict[key])


# CHAPTER 3

# 3.1.1
# When executing the function hsitogram_degree, the interaction file is read ...
